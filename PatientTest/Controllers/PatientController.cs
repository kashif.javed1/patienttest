﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PatientTest.Api.Core.Dto;
using PatientTest.Api.Core.Interfaces;


namespace PatientTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private IPatientRepository _patientRepository;
        private IWardRepository _wardRepository;

        public PatientController(IPatientRepository patientRepo, IWardRepository wardRepo)
        {
            this._patientRepository = patientRepo;
            this._wardRepository = wardRepo;
        }        

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var patients = await _patientRepository.GetAll();

            return Ok(patients);
        }

        [HttpPost]
        public async Task<IActionResult> Create(PatientCreate patientCreate) 
        {
            patientCreate.NHSNumber.Trim();

            patientCreate.Sex.Trim();

            if (patientCreate.Sex.ToUpper() != "FEMALE" && patientCreate.Sex.ToUpper() != "MALE")
            {
                return BadRequest("Sex should either be 'FEMALE' or 'MALE'");
            }

            var existingPatient = await _patientRepository.GetOnePatientByNHSNumber(patientCreate.NHSNumber);

            if (existingPatient != null)
            {
                return BadRequest("A patient with that NHS number already exists");
            }

            return Ok(await _patientRepository.Create(patientCreate));
        }

        [HttpPut("{patientId}")]
        public async Task<IActionResult> UpdatePatient(int patientId, PatientUpdate patientUpdate)
        {
            if (!CheckOneFieldIsBeingEdited(patientUpdate))
            {
                return BadRequest("Two of the three fields must be null");
            }

            if (patientId <= 0)
            {
                return BadRequest("Please enter a valid patient ID");
            }

            var patientToUpdate = await _patientRepository.GetOnePatient(patientId);

            if (patientToUpdate == null)
            {
                return BadRequest("That patient does not exist. Please enter a valid patient ID");
            }

            if (patientUpdate.DateOfDeath != null)
            {
                if (patientToUpdate.DateOfDeath != null)
                {
                    return BadRequest("This patient already has a date of death assigned to them");
                }
            }

            if (patientUpdate.DateOfDischarge != null)
            {
                if (patientToUpdate.DateOfDischarge != null)
                {
                    return BadRequest("This patient already has a date of discharge assigned to them");
                }
            }

            if (patientUpdate.WardId != null)
            {
                var existingWard = await _wardRepository.GetOne(patientUpdate.WardId);

                if (existingWard == null)
                {
                    return NotFound("No ward with that ID exists");
                }

                var patientsOnWard = await _patientRepository.GetPatientsOnWard(patientUpdate.WardId);

                if (patientsOnWard.Count() >= existingWard.Capacity)
                {
                    return BadRequest("That wards capacity has alredy been reached");
                }

                if (patientToUpdate.DateOfDeath != null || patientToUpdate.DateOfDischarge != null)
                {
                    return BadRequest("Unable to transfer this patient to the ward as they have either been discharged or are deceased");
                }

                if (existingWard.MixedSex)
                {
                    return Ok(await _patientRepository.UpdatePatient(patientId, patientUpdate));
                }
                else if (!existingWard.MixedSex)
                {
                    if (patientsOnWard.Count() <= 0)
                    {
                        return Ok(await _patientRepository.UpdatePatient(patientId, patientUpdate));
                    }

                    if (patientsOnWard.First().Sex == patientToUpdate.Sex)
                    {
                        return Ok(await _patientRepository.UpdatePatient(patientId, patientUpdate));
                    }
                }

                return BadRequest($"Unable to transfer {patientToUpdate.Sex.ToLower().Trim()} patient to a {patientsOnWard.First().Sex.ToLower().Trim()} ward");
            }

            return Ok(await _patientRepository.UpdatePatient(patientId, patientUpdate));
        }

        #region HelperFunctions
        private bool CheckOneFieldIsBeingEdited(PatientUpdate patientUpdate)
        {
            if (patientUpdate.DateOfDeath != null && (patientUpdate.DateOfDischarge != null || patientUpdate.WardId != null))
            {
                return false;                
            }
            else if (patientUpdate.DateOfDischarge != null && patientUpdate.WardId != null)
            {
                return false;
            }
            else if (patientUpdate.WardId == null && patientUpdate.DateOfDischarge == null && patientUpdate.DateOfDeath == null)
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}

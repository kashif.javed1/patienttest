﻿using System;
using System.ComponentModel.DataAnnotations;
using PatientTest.Api.Core.Enums;

namespace PatientTest.Api.Core.Models
{
    public class Patient
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter an NHS number")]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "The NHS number must be 16 digits long")]
        public string NHSNumber { get; set; }

        [Required(ErrorMessage = "You must enter a Surname")]
        [MaxLength(100, ErrorMessage = "Surname must be less than 100 characters")]
        public string Surname { get; set; }

        [Required(ErrorMessage ="You must enter a Forename")]
        [MaxLength(100, ErrorMessage = "Surname must be less than 100 characters")]
        public string Forename { get; set; }

        public string Sex { get; set; }

        [Required(ErrorMessage ="You must enter a DOB")]
        public DateTime DateOfBirth { get; set; }

        public DateTime? DateOfDeath { get; set; }
        public DateTime? DateOfDischarge { get; set; }
        public int WardId { get; set; }
        public string Status
        {
            get
            {
                if (WardId != 0)
                {
                    return StatusEnum.INCARE.ToString();
                }
                else if (DateOfDeath != null)
                {
                    return StatusEnum.DECEASED.ToString();
                }
                else if (DateOfDischarge != null)
                {
                    return StatusEnum.DISCHARGED.ToString();
                }

                return StatusEnum.NONE.ToString();
            }
        }
    }   
}


﻿namespace PatientTest.Api.Core.Models
{
    public class Ward
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool MixedSex { get; set; }
        public int Capacity { get; set; }
    }
}

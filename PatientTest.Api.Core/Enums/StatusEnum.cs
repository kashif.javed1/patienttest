﻿namespace PatientTest.Api.Core.Enums
{
    public enum StatusEnum
    {
        INCARE = 1, DECEASED = 2, DISCHARGED = 3, NONE = 4
    }
}

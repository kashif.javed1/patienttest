﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PatientTest.Api.Core.Dto
{
    public class PatientCreate
    {
        [Required(ErrorMessage = "You must enter an NHS number")]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "The NHS number must be 16 digits long")]
        public string NHSNumber { get; set; }

        [Required(ErrorMessage = "You must enter a Surname")]
        [MaxLength(100, ErrorMessage = "Surname must be less than 100 characters")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "You must enter a Forename")]
        [MaxLength(100, ErrorMessage = "Surname must be less than 100 characters")]
        public string Forename { get; set; }

        [Required(ErrorMessage = "You must enter a Sex")]
        public string Sex { get; set; }

        [Required(ErrorMessage = "You must enter a DOB")]
        public DateTime DateOfBirth { get; set; }
    }
}

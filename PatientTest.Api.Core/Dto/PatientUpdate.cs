﻿using System;

namespace PatientTest.Api.Core.Dto
{
    public class PatientUpdate
    {
        public DateTime? DateOfDeath { get; set; }
        public DateTime? DateOfDischarge { get; set; }
        public int? WardId { get; set; }
    }
}

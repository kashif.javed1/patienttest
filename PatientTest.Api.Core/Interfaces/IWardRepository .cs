﻿using System.Threading.Tasks;
using PatientTest.Api.Core.Models;

namespace PatientTest.Api.Core.Interfaces
{
    public interface IWardRepository
    {
        Task<Ward> GetOne(int? wardId);
    }
}

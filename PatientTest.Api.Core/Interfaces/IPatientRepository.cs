﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatientTest.Api.Core.Dto;
using PatientTest.Api.Core.Models;

namespace PatientTest.Api.Core.Interfaces
{
    public interface IPatientRepository
    {
        Task<IEnumerable<Patient>> GetAll();
        Task<Patient> GetOnePatient(int patientId);
        Task<Patient> GetOnePatientByNHSNumber(string NHSNumber);
        Task<Patient> Create(PatientCreate patientCreate);
        Task<IEnumerable<Patient>> GetPatientsOnWard(int? wardId);
        Task<Patient> UpdatePatient(int patientId, PatientUpdate patientUpdate);
    }
}

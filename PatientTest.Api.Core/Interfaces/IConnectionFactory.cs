﻿using System.Data;

namespace PatientTest.Api.Core.Interfaces
{
    public interface IConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}

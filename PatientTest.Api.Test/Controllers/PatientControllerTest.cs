using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PatientTest.Api.Controllers;
using PatientTest.Api.Core.Dto;
using PatientTest.Api.Core.Interfaces;
using PatientTest.Api.Core.Models;
using Xunit;

namespace PatientTest.Api.Test.Controllers
{
    public class PatientControllerTest
    {
        #region Variables and Constructor

        private Mock<IPatientRepository> _mockRepo;
        private Mock<IWardRepository> _mockWardRepo;
        private List<Patient> _patientList;
        private Ward _testWard;
        private PatientUpdate _testPatientUpdate;
        private PatientCreate _testPatientCreate;

        public PatientControllerTest()
        {
            _mockRepo = new Mock<IPatientRepository>();
            _mockWardRepo = new Mock<IWardRepository>();

            _testPatientUpdate = new PatientUpdate();

            _testPatientCreate = new PatientCreate()
            {
                NHSNumber = "1478523697894563",
                Surname = "testSurname",
                Forename = "testForename",
                Sex = "FEMALE",
                DateOfBirth = DateTime.Now
            };

            _patientList = new List<Patient>()
            {
                new Patient()
                {
                    Id = 1,
                    NHSNumber = "0000000000000001",
                    Surname = "Sanchez",
                    Forename = "Rick",
                    Sex = "MALE",
                    DateOfBirth = DateTime.Now,
                    DateOfDeath = null,
                    DateOfDischarge = null,
                    WardId = 1,
                },
                new Patient()
                {
                    Id = 2,
                    NHSNumber = "0000000000000002",
                    Surname = "Smith",
                    Forename = "Morty",
                    Sex = "MALE",
                    DateOfBirth = DateTime.Now,
                    DateOfDeath = null,
                    DateOfDischarge = null,
                    WardId = 1,
                },
                new Patient()
                {
                    Id = 3,
                    NHSNumber = "0000000000000003",
                    Surname = "Smith",
                    Forename = "Summer",
                    Sex = "FEMALE",
                    DateOfBirth = DateTime.Now,
                    DateOfDeath = null,
                    DateOfDischarge = null,
                    WardId = 1,
                },
                new Patient()
                {
                    Id = 4,
                    NHSNumber = "0000000000000004",
                    Surname = "Smith",
                    Forename = "Beth",
                    Sex = "FEMALE",
                    DateOfBirth = DateTime.Now,
                    DateOfDeath = null,
                    DateOfDischarge = null,
                    WardId = 1,
                }
            };

            _testWard = new Ward()
            {
                Id = 1,
                Code = "C3",
                Name = "ward",
                MixedSex = true,
                Capacity = 5
            };
        }
        #endregion

        #region GetAll
        [Fact]
        public async Task GetAll_PopulatedPatientList_ReturnsPatientList()
        {
            //Arrange
            var expected = GetListOfPatients(_patientList).Result.ToList();

            _mockRepo
                .Setup(x => x.GetAll())
                .Returns(GetListOfPatients(_patientList));
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.GetAll();
            var value = (actual as OkObjectResult).Value as List<Patient>;

            //Assert
            Assert.NotNull(value);
            Assert.IsType<OkObjectResult>(actual);
            Assert.Equal(expected.Count(), value.Count());

            for (var i = 0; i < value.Count; i++)
            {
                ComparePatients(expected[i], value[i]);
            }
        }

        [Fact]
        public async Task GetAll_EmptyPatientList_ReturnsEmptyList()
        {
            //Arrange
            var expected = new List<Patient>();

            _mockRepo
                .Setup(x => x.GetAll())
                .Returns(GetListOfPatients(new List<Patient>()));
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.GetAll();
            var value = (actual as OkObjectResult).Value as List<Patient>;

            //Assert
            Assert.NotNull(value);
            Assert.IsType<OkObjectResult>(actual);
            Assert.Equal(expected.Count(), value.Count());
        }
        #endregion

        #region Create
        [Fact]
        public async Task Create_ValidPatientModel_ReturnsOkStatusAndNewPatient()
        {
            //Arrange
            var expected = _patientList.First();
            _mockRepo
                .Setup(x => x.GetOnePatientByNHSNumber(_patientList.First().NHSNumber))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.Create(_testPatientCreate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.Create(_testPatientCreate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task Create_PatientWithNHSNumberAlreadyExists_ReturnsBadRequest()
        {
            //Arrange
            _testPatientCreate.NHSNumber = _patientList.First().NHSNumber;

            _mockRepo
                .Setup(x => x.GetOnePatientByNHSNumber(_patientList.First().NHSNumber))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.Create(_testPatientCreate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task Create_PatientCreateHasIncorrectSex_ReturnsBadRequest()
        {
            //Arrange
            _testPatientCreate.Sex = "NOTMALE";
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.Create(_testPatientCreate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }
        #endregion

        #region Update
        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelToMixedWard_ReturnsOkStatusAndUpdatedPatient()
        {
            //Arrange
            var expected = _patientList.First();

            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            _mockRepo
                .Setup(x => x.UpdatePatient(_patientList.First().Id, _testPatientUpdate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task UpdatePatient_WardCapacityReached_ReturnsBadRequest()
        {
            //Arrange
            _testWard.Capacity = 4;
            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_PatientAlreadyDischarged_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.WardId = 2;
            _patientList.First().DateOfDischarge = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_PatientAlreadyDeceased_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.WardId = 2;
            _patientList.First().DateOfDeath = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelToNonMixedWard_ReturnsOkStatusAndUpdatedPatient()
        {
            //Arrange
            var expected = _patientList.First();

            _testWard.MixedSex = false;
            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            _mockRepo
                .Setup(x => x.UpdatePatient(_patientList.First().Id, _testPatientUpdate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelToEmptyNonMixedWard_ReturnsOkStatusAndUpdatedPatient()
        {
            //Arrange
            var expected = _patientList.First();

            _testWard.MixedSex = false;
            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(new List<Patient>()));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            _mockRepo
                .Setup(x => x.UpdatePatient(_patientList.First().Id, _testPatientUpdate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelToNonMixedWardButIncorrectGender_ReturnsBadRequest()
        {
            //Arrange
            _testWard.MixedSex = false;
            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList[2]);
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync(_testWard);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelToNonExistantWard_ReturnsNotFound()
        {
            //Arrange
            _testPatientUpdate.WardId = 1;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.GetPatientsOnWard(It.IsAny<int>()))
                .Returns(GetListOfPatients(_patientList));
            _mockWardRepo
                .Setup(x => x.GetOne(It.IsAny<int>()))
                .ReturnsAsync((Ward)null);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<NotFoundObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelDateOfDeath_ReturnsOkStatusAndUpdatedPatient()
        {
            //Arrange
            var expected = _patientList.First();

            _testPatientUpdate.DateOfDeath = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.UpdatePatient(_patientList.First().Id, _testPatientUpdate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task UpdatePatient_PatientHasExistingDateOfDeath_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.DateOfDeath = DateTime.Now;
            _patientList.First().DateOfDeath = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_ValidPatientUpdateModelDateOfDischarge_ReturnsOkStatusAndUpdatedPatient()
        {
            //Arrange
            var expected = _patientList.First();

            _testPatientUpdate.DateOfDischarge = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            _mockRepo
                .Setup(x => x.UpdatePatient(_patientList.First().Id, _testPatientUpdate))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);
            var value = (actual as OkObjectResult).Value as Patient;

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(actual);
            ComparePatients(expected, value);
        }

        [Fact]
        public async Task UpdatePatient_PatientHasExistingDateOfDischarge_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.DateOfDischarge = DateTime.Now;
            _patientList.First().DateOfDischarge = DateTime.Now;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync(_patientList.First());
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_OneUpdtedPatientFieldNull_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.WardId = 2;
            _testPatientUpdate.DateOfDischarge = DateTime.Now;
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_InvalidPatientId_ReturnsBadRequest()
        {
            //Arrange
            _testPatientUpdate.WardId = 2;
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(0, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task UpdatePatient_PatientToUpdateIsNull_ReturnsBadRequest()
        {
            //Arrange
            var expected = _patientList.First();

            _testPatientUpdate.WardId = 2;

            _mockRepo
                .Setup(x => x.GetOnePatient(_patientList.First().Id))
                .ReturnsAsync((Patient)null);
            var controller = new PatientController(_mockRepo.Object, _mockWardRepo.Object);

            //Act
            var actual = await controller.UpdatePatient(_patientList.First().Id, _testPatientUpdate);

            //Assert
            Assert.NotNull(actual);
            Assert.IsType<BadRequestObjectResult>(actual);
        }
        #endregion

        #region Helper Functions
        public async Task<IEnumerable<Patient>> GetListOfPatients(List<Patient> listOfPatients)
        {
            return await Task.FromResult(listOfPatients);
        }

        internal void ComparePatients(Patient expected, Patient actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.NHSNumber, actual.NHSNumber);
            Assert.Equal(expected.Surname, actual.Surname);
            Assert.Equal(expected.Forename, actual.Forename);
            Assert.Equal(expected.Sex, actual.Sex);
            Assert.Equal(expected.DateOfBirth, actual.DateOfBirth);
            Assert.Equal(expected.DateOfDeath, actual.DateOfDeath);
            Assert.Equal(expected.DateOfDischarge, actual.DateOfDischarge);
            Assert.Equal(expected.WardId, actual.WardId);
        }
        #endregion
    }
}

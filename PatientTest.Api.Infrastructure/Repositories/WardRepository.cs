﻿using System.Threading.Tasks;
using Dapper;
using PatientTest.Api.Core.Interfaces;
using PatientTest.Api.Core.Models;

namespace PatientTest.Api.Infrastructure.Repositories
{
    public class WardRepository : IWardRepository
    {
        private IConnectionFactory _connection;

        public WardRepository(IConnectionFactory connection)
        {
            this._connection = connection;
        }

        public async Task<Ward> GetOne (int? wardId)
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        SELECT Id,
                               Name,
                               Code,
                               MixedSex,
                               Capacity
                          FROM Ward 
                         WHERE Id = @WardId";

                return await connection.QueryFirstOrDefaultAsync<Ward>(sqlQuery, new { WardId = wardId });
            }
        }
    }
}

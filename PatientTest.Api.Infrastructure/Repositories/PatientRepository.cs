﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using PatientTest.Api.Core.Dto;
using PatientTest.Api.Core.Interfaces;
using PatientTest.Api.Core.Models;

namespace PatientTest.Api.Infrastructure.Repositories
{
    public class PatientRepository : IPatientRepository
    {
        private IConnectionFactory _connection;

        public PatientRepository(IConnectionFactory connection)
        {
            this._connection = connection;
        }

        public async Task<IEnumerable<Patient>> GetAll()
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        SELECT Id,
                               NHSNumber,
                               Surname,
                               Forename,
                               Sex,
                               DateOfBirth,
                               DateOfDeath,
                               DateOfDischarge,
                               WardId
                          FROM Patient";

                return await connection.QueryAsync<Patient>(sqlQuery);
            }
        }

        public async Task<Patient> Create(PatientCreate patientCreate)
        {
            patientCreate.DateOfBirth = patientCreate.DateOfBirth.Date;

            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                   INSERT INTO Patient
                             ( NHSNumber, 
                               Surname, 
                               Forename, 
                               Sex, 
                               DateOfBirth, 
                               DateOfDeath, 
                               DateOfDischarge, 
                               WardId
                             )
                        VALUES 
                             ( @NHSNumber, 
                               @Surname, 
                               @Forename, 
                               @Sex, 
                               @DateOfBirth, 
                               null, 
                               null, 
                               null
                             )
                        SELECT Id,
                               NHSNumber,
                               Surname,
                               Forename,
                               Sex,
                               DateOfBirth,
                               DateOfDeath,
                               DateOfDischarge,
                               WardId 
                          FROM Patient 
                         WHERE Id = SCOPE_IDENTITY()";

                return await connection.QueryFirstOrDefaultAsync<Patient>(sqlQuery, patientCreate);
            }
        }

        public async Task<Patient> UpdatePatient(int patientId, PatientUpdate patientUpdate)
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        UPDATE Patient 
                           SET DateOfDischarge = @dateOfDischarge,
                               DateOfDeath = @dateOfDeath,
                               WardId = @wardId
                         WHERE Id = @id
                        SELECT Id,
                               NHSNumber,
                               Surname,
                               Forename,
                               Sex,
                               DateOfBirth,
                               DateOfDeath,
                               DateOfDischarge,
                               WardId 
                          FROM Patient 
                         WHERE Id = @id";

                return await connection.QueryFirstOrDefaultAsync<Patient>(sqlQuery, new { id = patientId, 
                                                                        dateOfDeath = patientUpdate.DateOfDeath, 
                                                                    dateOfDischarge = patientUpdate.DateOfDischarge, 
                                                                             wardId = patientUpdate.WardId });
            }
        }

        #region Helper Functions
        public async Task<IEnumerable<Patient>> GetPatientsOnWard(int? wardId)
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        SELECT Patient.Id, 
                               NHSNumber, 
                               Surname, 
                               Forename, 
                               Sex, 
                               DateOfBirth, 
                               DateOfDeath, 
                               DateOfDischarge, 
                               WardId
                          FROM Patient
                          JOIN Ward
                            ON Patient.WardId = Ward.Id
                         WHERE Ward.Id = @WardId";

                return await connection.QueryAsync<Patient>(sqlQuery, new { WardId = wardId });
            }
        }

        public async Task<Patient> GetOnePatient(int patientId)
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        SELECT Id,
                               NHSNumber,
                               Surname,
                               Forename,
                               Sex,
                               DateOfBirth,
                               DateOfDeath,
                               DateOfDischarge,
                               WardId 
                          FROM Patient 
                         WHERE Id = @PatientID";

                return await connection.QueryFirstOrDefaultAsync<Patient>(sqlQuery, new { PatientId = patientId });
            }
        }

        public async Task<Patient> GetOnePatientByNHSNumber(string NhsNumber)
        {
            using (var connection = this._connection.CreateConnection())
            {
                var sqlQuery = @"
                        SELECT Id,
                               NHSNumber,
                               Surname,
                               Forename,
                               Sex,
                               DateOfBirth,
                               DateOfDeath,
                               DateOfDischarge,
                               WardId 
                          FROM Patient 
                         WHERE NHSNumber = @NHSNumber";

                return await connection.QueryFirstOrDefaultAsync<Patient>(sqlQuery, new { NHSnumber = NhsNumber });
            }
        }
        #endregion
    }
}

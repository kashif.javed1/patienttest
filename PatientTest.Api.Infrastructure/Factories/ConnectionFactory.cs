﻿using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using PatientTest.Api.Core.Interfaces;

namespace PatientTest.Api.Infrastructure.Factories
{
    public class ConnectionFactory: IConnectionFactory
    {
        private readonly string _connectionString;

        public ConnectionFactory(IConfiguration config)
        {
            _connectionString = config.GetConnectionString("DbConnectionString");
        }

        public IDbConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
